#retrieval augmented mood

#from sentence_transformers import SentenceTransformer, util

class Compose:
    def __init__(self):
        self.fix_spelling = pipeline("text2text-generation",model="oliverguhr/spelling-correction-english-base")
        self.model = SentenceTransformer('sentence-transformers/all-MiniLM-L6-v2')

    def mood_detection(self, mood_prompt):
        mood_prompt = self.fix_spelling(mood_prompt)
        mood_states = ["I'm feeling a little nervous", "I'm feeling somewhat nervous", "I'm feeling really nervous",\
                       "I'm feeling sort of lack of interest", "I'm feeling somewhat lack of interest", "I'm feeling always lack of interest"\
                       "I'm feeling a little remorseful", "I'm feeling somewhat remorseful", "I'm feeling really remorseful"]


        close_mood = []
        mood_classes = {"I'm feeling a little nervous": "little Anxiexty", "I'm feeling somewhat nervous": "somewhat Anxiety", \
                        "I'm feeling really nervous": "real Anxiety", "I'm feeling sort of lack of interest": "little Depressed", \
                        "I'm feeling somewhat lack of interest": "somewhat Depressed", "I'm feeling always lack of interest": "real Depressed", \
                        "I'm feeling always lack of interest": "real Depressed", "I'm feeling a little remorseful": "little Guilt", \
                        "I'm feeling somewhat remorseful": "somewhat Guilty", "I'm feeling really remorseful": "real Guilt"}




        for mood_state in mood_states:
            embedding_1 = self.model.encode(mood_prompt, convert_to_tensor=True)
            embedding_2 = self.model.encode(mood_state, convert_to_tensor=True)
            dist = util.pytorch_cos_sim(embedding_1, embedding_2)
            close_mood.append(dist)

        mood_class = mood_states[close_mood.index(max(close_mood))]
        mood_class = mood_classes[mood_class]
        return(mood_class)



