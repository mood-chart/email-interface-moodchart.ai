from exchangelib import EWSDateTime
import pytz
from datetime import datetime
from tzlocal import get_localzone
from connectaccount import connect

from sentiment import sentiment


from barchart import barchart

from email_file_link import email_link
from upload_file import upload_file
from write_sub_bod import subbody
import shelve


import time

from cleanhtml import strip_tags

from email_file_link import email_link

account = connect()

my_folder = account.inbox

tz = get_localzone()


all_email = datetime(timenow.year, timenow.month, 4)
emailcount = account.inbox.filter(datetime_received__gt=all_email.astimezone(tz)).count()

chart_return = ''
bodsub_return = ''

for read in my_folder.all().order_by('-datetime_received')[0:emailcount]:
    sender = read.sender.email_address
    userdata = shelve.open('userdata.db')
    if sender == 'end-user-email-address':
      # print(read.subject)
      timestamp = str(read.datetime_received).split(' ')[0]
      timestamp = timestamp.split('-')[1]+'-'+timestamp.split('-')[2]
      subject = read.subject
      body = read.body
      body = strip_tags(body)
      mood = sentiment(subject)
      
      try:
        data = userdata[str(sender)]
        data.append((timestamp, mood, subject, body))
        userdata[str(sender)] = data
        print(len(data), '?')
        userdata.sync()
        
      except:
        userdata[sender] = [(timestamp, mood, subject, body)]
        data = userdata[sender]
        userdata.sync()
        
      subjectbody = []
      daymood = []

      for slices in data:
          subjectbody.append((slices[2], slices[3]))
          daymood.append((slices[0], slices[1]))
      
      

      
      
      print(daymood, 'hmmm')
      print(subject)
      chart = barchart(list(reversed(daymood)))
      print(chart)
      bodsub = subbody(list(reversed(subjectbody)))
      print(chart, "I'll see")
      chart_link = upload_file(chart)
      bodsub_link = upload_file(bodsub)
      time.sleep(5)
      print(chart_link)
      chart_return = chart_link
      bodsub_return = bodsub_link

    userdata.close()    


print(chart_return)
print(bodsub_return)
email_link(chart_return, bodsub_return, 'end-user-email-address', account)

account.protocol.close()
