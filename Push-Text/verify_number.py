import boto3 

def sns_init_verify(phone_number='+61482040275'):
    sns_client = boto3.client(
        service_name='sns',
        region_name='ap-southeast-2',
        aws_access_key_id='',
        aws_secret_access_key=''
    )

    response = sns_client.verify_sms_sandbox_phone_number(
    phone_number=phone_number
    )

    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        print('Phone number verified successfully')

    else:
        print('Error verifying phone number')




