    
import random
from subprocess import check_output
import time

def randfilename(r=False):
    if r == True:
        rand = [random.randint(0, 100) for d in range(10)]
        filename = ""
        for d in rand:
            filename+=str(d)

        return(filename)    

    out = check_output(["ls"])
    while True:
        rfile = randfilename(r=True)
        if rfile in str(out).split('\n'):
            rfile = randfilename(r=True)

        else:
            return(rfile)
            break

