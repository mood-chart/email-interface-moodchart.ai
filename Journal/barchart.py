import matplotlib.pyplot as plt
import numpy as np
import time

from randfile import randfilename

def barchart(x):
    #bar chart of positivity]
    print(x,"mang")
    filename = randfilename()
    dates = []
    data = []

    for j, (i, y, u, t) in enumerate(x):
        if i.day == 1:
            dates.append("#"+str(j)+"-"+str(i.day)+"st")

        elif i.day == 2:
            dates.append("#"+str(j)+"-"+str(i.day)+"nd")

        elif i.day == 3:
            dates.append("#"+str(j)+"-"+str(i.day)+"rd")

        elif i.day > 3 < 21:
            dates.append("#"+str(j)+"-"+str(i.day)+"th")

        elif i.day == 21:
            dates.append("#"+str(j)+"-"+str(i.day)+"st")

        elif i.day == 22:
            dates.append("#"+str(j)+"-"+str(i.day)+"nd")

        elif i.day == 23:
            dates.append("#"+str(j)+"-"+str(i.day)+"rd")


        elif i.day > 23 < 31:
            dates.append("#"+str(j)+"-"+str(i.day)+"th")


        elif i.day == 31:
            dates.append("#"+str(j)+"-"+str(i.day)+"st")


        print(i, 'g')
        data.append(y)



    y_pos = np.arange(len(dates))
    print(data, 'testv')

    plt.bar(y_pos, data, align='center', alpha=0.5)
    plt.xticks(y_pos, dates)
    plt.ylabel('Mood')
    plt.xlabel('Date')
    plt.savefig(filename+'bar')
    plt.clf()

    return(filename+'bar'+'.png')