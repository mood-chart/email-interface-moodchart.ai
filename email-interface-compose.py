import sys
sys.path.append('/email-interface-moodchart.ai/emai_client')
sys.path.append('/email-interface-moodchart.ai/file_services')
sys.path.append('/email-interface-moodchart.ai/Journal')
sys.path.append('/email-interface-moodchart.ai/neural_services')

#from email_interface_compose import Compose
from read_user_email import reademail
from scater_plot import scatter
from attach_email import attachfile
from text_journal import write_journal

import shelve
from datetime import date
import random
import re
from transformers import pipeline

from sentence_transformers import SentenceTransformer, util

class Compose:
    def __init__(self):
        self.fileid = [str(random.randint(0, 10)) for rand in range(15)]
        self.fix_spelling = pipeline("text2text-generation", model="oliverguhr/spelling-correction-english-base")
        self.database = shelve.open('mood.db')
        self.daynow = str(date.today().day)
        self.int_pattern = pattern = r'\d+'
        self.model = SentenceTransformer('sentence-transformers/all-MiniLM-L6-v2')

    def mood_detection(self, mood_prompt):
        mood_prompt = self.fix_spelling(mood_prompt)
        model = self.model
        mood_states = ["I'm feeling a little nervous", "I'm feeling somewhat nervous", "I'm feeling really nervous",\
                        "I'm feeling sort of lack of interest", "I'm feeling somewhat lack of interest", "I'm feeling really lack of interest", \
                        "I'm feeling a little remorseful", "I'm feeling somewhat remorseful", "I'm feeling really remorseful"]


        close_mood = []
        mood_classes = {"I'm feeling a little nervous": 5, "I'm feeling somewhat nervous": 15, \
                        "I'm feeling really nervous": 30, "I'm feeling sort of lack of interest": 50, \
                        "I'm feeling somewhat lack of interest": 75, "I'm feeling really lack of interest": 100, \
                        "I'm feeling a little remorseful": 30, \
                        "I'm feeling somewhat remorseful": 45, "I'm feeling really remorseful": 50}




        for mood_state in mood_states:
            embedding_1 = model.encode(mood_prompt, convert_to_tensor=True)
            embedding_2 = model.encode(mood_state, convert_to_tensor=True)
            dist = util.pytorch_cos_sim(embedding_1, embedding_2)
            close_mood.append(dist)

        mood_class = mood_states[close_mood.index(max(close_mood))]
        mood_class = mood_classes[mood_class]
        return(mood_class)







    def compose(self, email, username, password):
        emailcontents = reademail(email, username, password)
        contents = emailcontents[0][2].split('\n')
        
        mood_vector = contents[5].split('.')
        mood_vector = [self.mood_detection(mood_vector[mood]) for mood in range(3)]
        mood_vector = list(reversed(mood_vector))

"""

        #The five emotions, propotional to a business logic



        g = mood_vector[0]
        a = mood_vector[1]
        d = mood_vector[2]

        user = contents[0]
        try:
            data = self.database[user]
            data.append((g, a, d))
            self.database[user] = data
            self.database.sync()

        except:
            data = [(g, a, d)]
            self.database[user] = data
            self.database.sync()

        self.database.close()

        scatter(data, self.fileid)
            # write_journal(data)
            #attachfile(self.fileid)

"""
compose = Compose()