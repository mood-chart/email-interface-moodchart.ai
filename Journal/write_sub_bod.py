import random
from subprocess import check_output


from randfile import randfilename

def subbody(emailtext):
    rfile = randfilename()
    outfile = rfile+'content'+'.txt'
    etext = open('%s'%outfile, 'w')
    for i, (x, y, j, text) in enumerate(emailtext):
        etext.write("#"+str(i)+"-"+j+":"+"\n"+text)

    etext.close()
    return(outfile)
