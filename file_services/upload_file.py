import logging
import boto3
from botocore.exceptions import ClientError
import os


def upload_file(file_name, object_name=None):
    ackey = open('accesskey.txt','r').read()
    acsecret = open('accesssecret.txt', 'r').read()
    s3 = boto3.resource(
        service_name='s3',
        region_name='us-east-2',
        aws_access_key_id='',
        aws_secret_access_key=''
    )

    s3.Bucket('moodchart').upload_file(Filename=file_name, Key=file_name, ExtraArgs={'ACL':'public-read'})
    return('https://moodchart.s3.ap-southeast-2.amazonaws.com/%s'%file_name)

       
