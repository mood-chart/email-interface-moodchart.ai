from exchangelib import Credentials, Account, Configuration, DELEGATE, Message, Mailbox
import exchangelib

import os

def regular_email():
    devmod = open('config-dev.txt').read() #In dev mode don't send autotamed emails, with EC2 boot scheduler.
    if 'config-dev' in devmod:
        pass

    else:
        # Create an Exchangelib session object
        email_addresses = open('startups.txt').read().split('\n')
        email_addresses.pop(len(email_addresses)-1)

        connection = connect()
        # Get the list of email addresses to send to

        # Create a message object for each email address
        messages = []
        for address in email_addresses:
            message = exchangelib.Message(
                       account=connection,
                       subject="Reply: how are your KPIs doing?",
                       body="Please, reply to this email with your current performance w.r.t your KPIs... attached is your KPIs.",
                       to_recipients=[
                           Mailbox(email_address=address),
                           ]
                      )
            message.cc_recipients = ['roy@coachanywhere.com.au']
            messages.append(message)

           # Send the messages

        with open('kpi.csv', 'rb') as f:
            attachment = exchangelib.FileAttachment(name='action-kpis.csv', content=f.read())
            message.attach(attachment)

            for email in messages:
                message.send(email)




        connection.protocol.close()



