from exchangelib import Credentials, Account, Configuration, DELEGATE, Message, Mailbox


def connect(email, username, password):
    """
    Get Exchange account connection with server
    """
    creds = Credentials(username=username, password=password)
    config = Configuration(server='mail.premium.exchange', credentials=creds)
    return Account(primary_smtp_address=email, autodiscover=False,
                   config = config, access_type=DELEGATE)

