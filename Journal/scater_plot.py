from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from color_severity import color_code

def scatter(data, fileid):
    pointg = []
    pointa = []
    pointd = []


    for (g, a, d) in data:
        pointg.append(g)
        pointa.append(a)
        pointd.append(d)

    
    colors = ['red' for d in range(len(pointd))]


    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    img = ax.scatter(pointg, pointa, pointd, c=colors, cmap=plt.hot())
    fig.colorbar(img)
    plt.savefig('moodchart-%s.png'%fileid)
