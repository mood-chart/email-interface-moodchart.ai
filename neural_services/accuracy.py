import csv
from sentiment import sentiment



sentiments = []
with open('test.csv', newline='') as csvfile:
    emotions = csv.reader(csvfile, delimiter=' ', quotechar='|')
    accuracies = []
    for row in emotions:
        emote = ''
        if row[len(row)-1] == 'text,label':
            pass
        else:    
            emoteinteger = int(row[len(row)-1].split(',')[1])

        for emotee in row[0:len(row)-1]:
            emote = emote+' '+emotee
        

        sent = sentiment(emote)
        try:
            sentiments.append((sent, emoteinteger))
        
        except:
            pass

        emote = ''

        for hypothesis, groundtruth in sentiments:
            if groundtruth == 1 or groundtruth == 0:
                print(hypothesis, groundtruth)
                if hypothesis == -1 and groundtruth == 0:
                    accuracies.append(1) 

                elif hypothesis == -1 and groundtruth == 1:
                    accuracies.append(0)

                elif hypothesis == 2 and groundtruth == 1:
                    accuracies.append(1)

                elif hypothesis == 2 and groundtruth == 0:
                    accuracies.append(0)

                if sum(accuracies) > 0:
                    print(float((len(accuracies)-sum(accuracies))/sum(accuracies)))          
